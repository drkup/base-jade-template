var gulp = require('gulp'),
    jade = require('gulp-jade'),
    stylus = require('gulp-stylus'),
    connect = require('gulp-connect');

gulp.task('templates', function() {
    var YOUR_LOCALS = {};

    gulp.src(['./assets/*.jade'] )
        .pipe(jade({
            locals: YOUR_LOCALS
        }))
        .pipe(gulp.dest('./public/'))
        .pipe(connect.reload());
});

gulp.task('cssst', function () {
    gulp.src(['./assets/css/*.styl'],['./assets/blocks/**/*.styl'])
        .pipe(stylus())
        .pipe(gulp.dest('./public/css/'))
        .pipe(connect.reload());
});

gulp.task('connect', function() {
    connect.server({
        root: 'app',
        livereload: true
    });
});

gulp.task('watch', function () {

    gulp.watch(['./assets/*.jade', './assets/blocks/**/*.jade'], ['templates']);
    gulp.watch(['./assets/css/*.styl','./assets/blocks/**/*.styl'], ['cssst']);
});

gulp.task('connectDev', function () {
    connect.server({
        root: ['public', 'tmp'],
        port: 8000,
        livereload: true
    });
});

gulp.task('server', [ 'templates', 'cssst', 'connectDev', 'watch']);




